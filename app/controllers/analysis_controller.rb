require 'nbayes'

class AnalysisController < ApplicationController
  layout "main"

  def analyze
    @issue = params[:text]

    #Creating instances of Naive Bayes classifiers for Referential Emotive, Conative and Phatic functions
    nbayesR = NBayes::Base.new
    nbayesE = NBayes::Base.new
    nbayesC = NBayes::Base.new
    nbayesP = NBayes::Base.new

    #Training

    @CommentsTrain = Comment.all

    @CommentsTrain.each do |comment|
      if comment.referential then labelR= "Yes" else labelR = "No" end
      if comment.emotive then labelE= "Yes" else labelE = "No" end
      if comment.conative then labelC= "Yes" else labelC = "No" end
      if comment.phatic then labelP= "Yes" else labelP = "No" end


      tokens= tokenize(comment.comment)
      nbayesR.train(tokens, labelR)
      nbayesE.train(tokens, labelE)
      nbayesC.train(tokens, labelC)
      nbayesP.train(tokens, labelP)
    end

    #Classification
    issueTokenized = tokenize(@issue)
    @resultR = nbayesR.classify(issueTokenized).max_class
    @resultC = nbayesC.classify(issueTokenized).max_class
    @resultE = nbayesE.classify(issueTokenized).max_class
    @resultP = nbayesP.classify(issueTokenized).max_class



  end


  def tokenize(stringToSplit)
    tokens = stringToSplit.split + wngrams(2,stringToSplit) + wngrams(3,stringToSplit) + chngrams(2,stringToSplit) + chngrams(3,stringToSplit)
    return tokens
  end
  

  def wngrams(n, text)
    iMin=0
    iMax=n-1
    tokens = []

    tokenList = text.split
    tokenList.each do
      if(iMin+n <= tokenList.length()) then
        tokens.append((tokenList[iMin..iMax]).join(" "))
        iMin += 1
        iMax += 1
      end
    end
    return  tokens
  end

  def chngrams(n, text)
    iMin=0
    iMax=n-1
    tokens = []
    while iMin+n <= text.length()  do
        tokens.append(text[iMin..iMax])
        iMin += 1
        iMax += 1
    end
    return  tokens
  end

end

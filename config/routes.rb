Rails.application.routes.draw do

  resources :example
  get 'analysis/index', to: 'analysis#index'
  get 'analysis/', to: 'analysis#index'
  post 'analysis/analyze', to: 'analysis#analyze'

end

class CreateComments < ActiveRecord::Migration[6.1]
  def change
    create_table :comments do |t|
      t.bigint :id_issue
      t.text :comment
      t.string :project
      t.boolean :referential
      t.boolean :emotive
      t.boolean :conative
      t.boolean :phatic

      t.timestamps
    end
  end
end
